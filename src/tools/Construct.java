/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import graphtheory.GUI;
import graphtheory.Stock;
import graphtheory.Vertex;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author Marx
 */
public class Construct extends Application {
    private int cycle = 0;
    private Pane root;
    private ArrayList<Node> items = new ArrayList<Node>();

    public void start(Stage primaryStage) {
        root = new Pane();

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("CONSTRUCTION PANE");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        root.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        if (mouseEvent.isControlDown()) {
                            // ctr+click
                            root.getChildren().remove(items.get(items.size()-1));
                        } else if (mouseEvent.isSecondaryButtonDown()) {
                            // right click
                            cycle++;
                            System.out.println("Cycle: " + cycle % 5);
                            System.out.println("Scene.setHeight(" + primaryStage.getHeight() + ")");
                            System.out.println("Scene.setWidth(" + primaryStage.getWidth() + ")");
                        } else {
                            // event left click
                            if(cycle % 5 == 0) {
                                Label d = new Label("DEFAULT WORDS:");
                                System.out.println("Label.setLayoutX(" + mouseEvent.getX() + ")");
                                System.out.println("Label.setLayoutY(" + mouseEvent.getY() + ")");
                                d.setLayoutX(mouseEvent.getX());
                                d.setLayoutY(mouseEvent.getY());
                                root.getChildren().add(d);
                                items.add(d);
                            }
                            // event left click
                            if(cycle % 5 == 1) {
                                TextField d = new TextField ("200");
                                System.out.println("TextField.setLayoutX(" + mouseEvent.getX() + ")");
                                System.out.println("TextField.setLayoutY(" + mouseEvent.getY() + ")");
                                d.setLayoutX(mouseEvent.getX());
                                d.setLayoutY(mouseEvent.getY());
                                root.getChildren().add(d);
                                items.add(d);
                            }
                        }
                    }
                });
        /*
        root.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.UP || event.getCode() == KeyCode.DOWN
                        || event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT) {
                    System.out.println("KEY");
                    event.consume();
                }
            }
        });
                */
       
    }
    public static void main(String[] args) {
        GUI.launch(args);
    }
}
