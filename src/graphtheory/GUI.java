/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphtheory;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import plugins.Plugin;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 *
 * @author Marx
 */
public class GUI extends Application {

    public static GUI app;

    private ArrayList<Vertex> vertices = new ArrayList<Vertex>();
    private ArrayList<Vertex> selectedVertices = new ArrayList<Vertex>();
    private ArrayList<Tab> tabs = new ArrayList<Tab>();
    
    private Pane root;
    private Pane nodePane;
    private Pane tabPane;
    
    private ArrayList<int[]> selectedStart = new ArrayList<int[]>();
    boolean isDragging = false;
    boolean canBuildVertex = true;
    private Rectangle selectorTool;
    private int[] selectorToolStart;
    private int[] nodeDragStart;
    
    private ArrayList<Plugin> plugins;

    public Stage primaryStage;
    
    private int currentTab = 0;
    
    public void start(Stage primaryStage) {
        app = this;
        root = new Pane();
        this.primaryStage = primaryStage;
        newTab();
        
        Scene scene = new Scene(root, Stock.APP_SIZE[0], Stock.APP_SIZE[1]);

        primaryStage.setTitle(Stock.APP_TITLE);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        // Construct tool bar
        plugins = Plugin.loadPlugins(); // Plugins after all initializing because they may need elements from this class

        // Build left infoPane
        buildInfoPane();
        
        MenuBar menuBar = new MenuBar();

        //menuBar.setStyle("-fx-background-color: #212121");
        // --- Menu File
        Menu[] menus = new Menu[Plugin.PLUGIN_TYPES.length];
        
        for(int i = 0; i < menus.length; i++) {
            menus[i] = new Menu(Plugin.PLUGIN_TYPES[i]);
            menuBar.getMenus().add(menus[i]);
        }
        
        MenuItem save = new MenuItem("Save Tab");
        menus[Plugin.PLUGIN_FILE].getItems().add(save);
        MenuItem load = new MenuItem("Load Tab");
        menus[Plugin.PLUGIN_FILE].getItems().add(load);
        
        save.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                saveFile();
                alertPluginsUpdate();
            }
        });
        load.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                openFile();
                alertPluginsUpdate();
            }
        });
        
        Menu blank = new Menu();
        blank.setText(Stock.MENU_SPACING);
        blank.setDisable(true);
        menuBar.getMenus().add(blank);
        for (Plugin plugin : plugins) {
            if(plugin.getType() != Plugin.PLUGIN_DEFAULT && plugin.getType() != Plugin.PLUGIN_OTHER) {
                MenuItem pluginItem = new MenuItem(plugin.getPluginName());
                menus[plugin.getType()].getItems().add(pluginItem);

                pluginItem.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent t) {
                    plugin.run();
                    alertPluginsUpdate();
                }
            }); 
        }
        }
        root.getChildren().add(menuBar);
    }

    public void handleLeftClick(MouseEvent mouseEvent) {
        for (int i = 0; i < plugins.size(); i++) {
            if (plugins.get(i).handleClick(mouseEvent) == true) {
                return;
            }
        }

        // If you're dragging an a vertex lets not try to make new ones or connect it to anything
        if (isDragging) {
            return;
        }

        // Check if user click on something or space
        int s = -1; // Non existant
        for (int i = 0; i < getVertices().size(); i++) {
            if (getVertices().get(i).isClickingOnImage(mouseEvent)) {
                s = i;
            }
        }
        if (s > -1) {
            // Clicking on S

            if (getSelectedVertices().isEmpty()) {
                // Nothing is currently selected, lets select S

                getSelectedVertices().add(getVertices().get(s));
                addStartPosition(getVertices().get(s));
                getVertices().get(s).setSelected(true);
            } else {
                // Stuff is selected, lets connect all of them to S and deselect them
                for (int i = 0; i < getSelectedVertices().size(); i++) {
                    getSelectedVertices().get(i).addEdge(getVertices().get(s), nodePane);
                    getSelectedVertices().get(i).setSelected(false);
                }
                selectedVertices = new ArrayList<Vertex>();
                clearStartPosition();
                alertPluginsUpdate();
                canBuildVertex = false;
            }

        } else {
            // Clicking in space
            /*
            if (getSelectedVertices().isEmpty()) {
                // Create a vertice
                Vertex V = new Vertex((int) mouseEvent.getX(), (int) mouseEvent.getY(), getVertices().size());
                nodePane.getChildren().add(V);
                getVertices().add(V);
                alertPluginsUpdate();
            }
            */
        }
    }

    public void handleRightClick(MouseEvent mouseEvent) {
        System.out.println("Right Click");
        plugins.get(0).run();
    }

    public void handleCtrlClick(MouseEvent mouseEvent) {
        int selectedVertex = -1; // Non existant
        for (int i = 0; i < getVertices().size(); i++) {
            if (getVertices().get(i).isClickingOnImage(mouseEvent)) {
                selectedVertex = i;
            }
        }
        if (selectedVertex > -1) {
            getSelectedVertices().add(getVertices().get(selectedVertex));
            addStartPosition(getVertices().get(selectedVertex));
            getVertices().get(selectedVertex).setSelected(true);
        }
    }

    public void handleMouseRelease(MouseEvent mouseEvent) {
        if(selectorTool != null) {
            // Check if its selecting anything
            for (Vertex vertice : vertices) {
                if (vertice.isInBox(selectorTool)) {
                    selectedVertices.add(vertice);
                    addStartPosition(vertice);
                    vertice.setSelected(true);
                }
            }
            
            
            nodePane.getChildren().remove(selectorTool);
            selectorTool = null;
            return;
        }
        
        if (isDragging) {
            isDragging = false;
            for (int i = 0; i < getSelectedVertices().size(); i++) {
                getSelectedVertices().get(i).setSelected(false);
            }
            selectedVertices = new ArrayList<Vertex>();
            clearStartPosition();
        } else {
            if (getSelectedVertices().isEmpty() && canBuildVertex) {
                // Create a vertice
                Vertex V = new Vertex((int) mouseEvent.getX(), (int) mouseEvent.getY(), getVertices().size());
                nodePane.getChildren().add(V);
                getVertices().add(V);
                alertPluginsUpdate();
            } else {
                if(getSelectedVertices().size() != 1)
                    deselectAll();
            }
        }
        canBuildVertex = true;
    }

    public void handleDrag(MouseEvent mouseEvent) {
        if (getSelectedVertices().isEmpty()) {
            // Selecter Square will go here
            //System.out.println("Drag square");
            if (selectorTool == null) {
                selectorToolStart = new int[2];
                selectorTool = new Rectangle();
                selectorTool.setOpacity(.10);
                selectorTool.toFront();
                selectorToolStart[0] = (int) mouseEvent.getX();
                selectorToolStart[1] = (int) mouseEvent.getY();
                nodePane.getChildren().add(selectorTool);
                return;
            } else {
                if(mouseEvent.getX() < selectorToolStart[0] && mouseEvent.getY() < selectorToolStart[1]) {
                    //System.out.println("NORTH WEST");
                    selectorTool.setLayoutX(mouseEvent.getX());
                    selectorTool.setLayoutY(mouseEvent.getY());
                }
                if(mouseEvent.getX() > selectorToolStart[0] && mouseEvent.getY() < selectorToolStart[1]) {
                    //System.out.println("NORTH EAST");
                    selectorTool.setLayoutX(selectorToolStart[0]);
                    selectorTool.setLayoutY(mouseEvent.getY());
                }
                if(mouseEvent.getX() < selectorToolStart[0] && mouseEvent.getY() > selectorToolStart[1]) {
                    //System.out.println("SOUTH WEST");
                    selectorTool.setLayoutX(mouseEvent.getX());
                    selectorTool.setLayoutY(selectorToolStart[1]);
                }
                if(mouseEvent.getX() > selectorToolStart[0] && mouseEvent.getY() > selectorToolStart[1]) {
                    //System.out.println("SOUTH EAST");
                    selectorTool.setLayoutX(selectorToolStart[0]);
                    selectorTool.setLayoutY(selectorToolStart[1]);
                }
                selectorTool.setWidth(Math.abs(selectorToolStart[0] - mouseEvent.getX()));
                selectorTool.setHeight(Math.abs(selectorToolStart[1] - mouseEvent.getY()));
                return;
            }
        } else {
            if(!isDragging) {
                nodeDragStart = new int[2];
                nodeDragStart[0] = (int)mouseEvent.getX();
                nodeDragStart[1] = (int)mouseEvent.getY();
            }
            isDragging = true;
            int DX = nodeDragStart[0] - (int)mouseEvent.getX();
            int DY = nodeDragStart[1] - (int)mouseEvent.getY();

            for (int i = 0; i < getSelectedVertices().size(); i++) {
                //Vertex v = getSelectedVertices().get(i);
                int NX = selectedStart.get(i)[0] - DX;
                int NY = selectedStart.get(i)[1] - DY;
               // if(NX > 0 && NX < (Stock.APP_SIZE[0] - Stock.INFO_SIZE) && NY > Stock.TAB_Y/2 && NY < Stock.APP_SIZE[1] - Stock.TAB_Y*2) 
                // getSelectedVertices().get(i).setLayoutPosition(NX, NY);
                
                if(NX < 0) {
                    NX = 0;
                } else if(NX > (Stock.APP_SIZE[0] - Stock.INFO_SIZE)) {
                    NX = (Stock.APP_SIZE[0] - Stock.INFO_SIZE);
                }
                if(NY < Stock.TAB_Y/2) {
                    NY = Stock.TAB_Y/2;
                } else if(NY > Stock.APP_SIZE[1] - Stock.TAB_Y*2) {
                    NY = Stock.APP_SIZE[1] - Stock.TAB_Y*2;
                }
                getSelectedVertices().get(i).setLayoutPosition(NX, NY);
            }
        }
    }

    public static void main(String[] args) {
        GUI.launch(args);
    }

    /**
     * @return the vertices
     */
    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    /**
     * @return the selectedVertices
     */
    public ArrayList<Vertex> getSelectedVertices() {
        return selectedVertices;
    }

    public Pane getRoot() {
        return root;
    }
    
    public Pane getNodePane() {
        return nodePane;
    }
    public void buildTabs() {
        if(tabPane != null)
            root.getChildren().remove(tabPane);
        
        tabPane = new Pane();
        
        // Build tabs
        Image tabIMG = new Image(Stock.IMAGE_TAB);
        for(int i = 0; i < tabs.size(); i++) {
            Pane cReg = new Pane();
            Label lbl = new Label(tabs.get(i).getTitle());
            System.out.println("Tab name: " + tabs.get(i).getTitle());
            ImageView view = new ImageView(tabIMG);;
            if(nodePane == tabs.get(i).getNodePane()) 
                view.setImage(new Image(Stock.IMAGE_TABSELECTED));
            
            view.setId("" + i);
            lbl.setLayoutY(view.getImage().getHeight() * 1/6);
            lbl.setLayoutX(view.getImage().getWidth() * .20);
            
            
            cReg.getChildren().add(view);
            cReg.getChildren().add(lbl);
            cReg.setLayoutX(i * view.getImage().getWidth());
            cReg.setLayoutY(Stock.TAB_Y);
            
            tabPane.getChildren().add(cReg);
            
            cReg.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        switchTab(Integer.parseInt(view.getId()));
                    }
            });
        }
        
        // New tab button
        ImageView newTab = new ImageView(new Image(Stock.IMAGE_NEWTAB));
        newTab.setLayoutY(Stock.TAB_Y);
        newTab.setLayoutX(tabs.size() * tabIMG.getWidth());
        tabPane.getChildren().add(newTab);
        root.getChildren().add(tabPane);
        tabPane.toBack();
            
        newTab.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    newTab();
                }
        });
    }
    public void newTab() {
        root.getChildren().remove(nodePane);
        
        tabs.add(new Tab(tabs.size()));
        vertices = tabs.get(tabs.size()-1).getVertices();
        nodePane = tabs.get(tabs.size()-1).getNodePane();
        
        
        root.getChildren().add(nodePane);
        
                nodePane.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        handleDrag(mouseEvent);
                    }
                });
        nodePane.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {                        
                        if (mouseEvent.isControlDown()) {
                            handleCtrlClick(mouseEvent);
                        } else if (mouseEvent.isSecondaryButtonDown()) {
                            handleRightClick(mouseEvent);
                        } else {
                            handleLeftClick(mouseEvent);
                        }
                    }
                });
        nodePane.addEventHandler(MouseEvent.MOUSE_RELEASED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        handleMouseRelease(mouseEvent);
                    }
                });
        
        buildTabs();
        currentTab = tabs.size() - 1;
        alertPluginsUpdate();
    }
    public void switchTab(int tab) {
        System.out.println("Switch to tab " + tabs.get(tab).getTitle());
        
        root.getChildren().remove(nodePane);
        currentTab = tab;
        vertices = tabs.get(tab).getVertices();
        nodePane = tabs.get(tab).getNodePane();
        
        
        root.getChildren().add(nodePane);
        buildTabs();
        alertPluginsUpdate();
    }
    public void buildInfoPane() {
        new InfoElement(root); // Create an instance for singleTon data structure to initiate
        
        // Central line
        Line l = new Line();
        l.setStartX(Stock.APP_SIZE[0] - (Stock.INFO_SIZE/3));
        l.setStartY(Stock.TAB_Y);
        l.setEndX(Stock.APP_SIZE[0] - (Stock.INFO_SIZE/3));
        l.setEndY(Stock.APP_SIZE[1]);
        root.getChildren().add(l);
        
        // Left most line
        l = new Line();
        l.setStartX(Stock.APP_SIZE[0] - (Stock.INFO_SIZE));
        l.setStartY(Stock.TAB_Y);
        l.setEndX(Stock.APP_SIZE[0] - (Stock.INFO_SIZE));
        l.setEndY(Stock.APP_SIZE[1]);
        root.getChildren().add(l);
        
        // Right line
        l = new Line();
        l.setStartX(Stock.APP_SIZE[0]-1);
        l.setStartY(Stock.TAB_Y);
        l.setEndX(Stock.APP_SIZE[0]-1);
        l.setEndY(Stock.APP_SIZE[1]);
        root.getChildren().add(l);
        
        alertPluginsUpdate();
        
        /*
        // Top line
        l = new Line();
        l.setStartX(Stock.APP_SIZE[0] - (Stock.INFO_SIZE));
        l.setStartY(Stock.TAB_Y + 1);
        l.setEndX(Stock.APP_SIZE[0]);
        l.setEndY(Stock.TAB_Y + 1);
        root.getChildren().add(l);
        
        // Bottom line
        l = new Line();
        l.setStartX(Stock.APP_SIZE[0] - (Stock.INFO_SIZE));
        l.setStartY(Stock.APP_SIZE[1] - 1);
        l.setEndX(Stock.APP_SIZE[0]);
        l.setEndY(Stock.APP_SIZE[1] - 1);
        root.getChildren().add(l);
        */
    }
    public void addStartPosition(Vertex v) {
        int[] xy = new int[2];
        xy[0] = (int) v.getMidX();
        xy[1] = (int) v.getMidY();
        selectedStart.add(xy);
    }
    public void clearStartPosition() {
        selectedStart = new ArrayList<int[]>();
    }
    public void alertPluginsUpdate() {
        if(plugins == null)
            return;
        
        for(int i = 0; i < plugins.size(); i++) {
            plugins.get(i).graphUpdate();
        }
    }
    public void deselectAll() {
        for (Vertex selectedVertice : selectedVertices) {
            selectedVertice.setSelected(false);
        }
        selectedVertices = new ArrayList<Vertex>();
        selectedStart = new ArrayList<int[]>();
    }
    public void saveFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("/"));
        fileChooser.setTitle("Save Current Graph File Tab");
        fileChooser.setInitialFileName(tabs.get(currentTab).getTitle());
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Graph Files", "*.graph");
        fileChooser.getExtensionFilters().add(filter);

        File choosen = fileChooser.showSaveDialog(primaryStage);
        
        if(choosen == null)
            return;
        
        String path = choosen.getAbsolutePath();
        System.out.println(path);
        System.out.println("From " + path.lastIndexOf('\\') + " to " + path.lastIndexOf('.'));
        String fileName = path.substring(path.lastIndexOf('\\')+1, path.lastIndexOf('.'));
        System.out.println(fileName);
        tabs.get(currentTab).setTitle(fileName);
        System.out.println(tabs.get(currentTab).getTitle());
        buildTabs();
        
        try {
            FileOutputStream fos = new FileOutputStream(choosen);
            DataOutputStream dos = new DataOutputStream(fos);
            // FIRST WRITE THE DIMENSIONS
            dos.writeInt(this.vertices.size()); // Size of vertices (will help later

            for (int i = 0; i < vertices.size(); i++) {
                dos.writeInt((int)vertices.get(i).getLayoutX());
                dos.writeInt((int)vertices.get(i).getLayoutY());

                String StrEdgeTo = "";
                for (int x = 0; x < vertices.get(i).getEdgeTo().size(); x++) {
                    StrEdgeTo = StrEdgeTo + " " + vertices.get(i).getEdgeTo().get(x).getI();
                }
                dos.writeUTF(StrEdgeTo);
                System.out.println(StrEdgeTo);
            }
            dos.close();
            System.out.println("Successful save");
        } catch (Exception e) {
            System.out.println("failed to save");
            e.printStackTrace();
        }
    }
    public void openFile() {
       FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("/"));
        fileChooser.setTitle("Open Graph File as New Tab");
        fileChooser.setInitialFileName(tabs.get(currentTab).getTitle());
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Graph Files", "*.graph");
        fileChooser.getExtensionFilters().add(filter);

        File choosen = fileChooser.showOpenDialog(primaryStage);
        
        if(choosen == null)
            return;
        
        
        
        String path = choosen.getAbsolutePath();
        System.out.println(path);
        System.out.println("From " + path.lastIndexOf('\\') + " to " + path.lastIndexOf('.'));
        String fileName = path.substring(path.lastIndexOf('\\')+1, path.lastIndexOf('.'));
        System.out.println(fileName);
        newTab();
        tabs.get(currentTab).setTitle(fileName);
        buildTabs();
        
        try {
            byte[] bytes = new byte[Long.valueOf(choosen.length()).intValue()];
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            FileInputStream fis = new FileInputStream(choosen);
            BufferedInputStream bis = new BufferedInputStream(fis);

            bis.read(bytes);
            bis.close();

            DataInputStream dis = new DataInputStream(bais);

            int verticesSize = dis.readInt();
            System.out.println("load " + verticesSize + " vertices");
            
            ArrayList<String> edgeTo = new ArrayList<>();
            
            for(int i = 0; i < verticesSize; i++) {
                int x = dis.readInt();
                int y = dis.readInt();
                Vertex v = new Vertex(x,y,i);
                v.setLayoutPosition(x + (int)(v.getPixSize()/2), y + (int)(v.getPixSize()/2));
                System.out.println("x " + x + " y" + y + " i " + i);
                //dis.readUTF()
                edgeTo.add(dis.readUTF());
                vertices.add(v);
                nodePane.getChildren().add(v);
            }
            for(int i = 0; i < verticesSize;i++) {
                String[] edgeToStr = edgeTo.get(i).split(" ");
                System.out.println(edgeTo.get(i));
                for(String id:edgeToStr) {
                    if(!id.equals("")) {
                        int cid = Integer.parseInt(id);
                        vertices.get(i).addEdge(vertices.get(cid), nodePane);
                    }
                }
            }
            System.out.println("Successful load");
        } catch (Exception e) {
            System.out.println("Failed to load");
            e.printStackTrace();
        }
    }
}
