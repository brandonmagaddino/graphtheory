/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphtheory;

import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Marx
 */
public class Vertex extends Pane {
    private static final String selectedImage = Stock.IMAGE_FOLDER + "vertex/vertext_sel_outline.png";
    private static final String unselectedImage = Stock.IMAGE_FOLDER + "vertex/vertext_outline.png";
    private static final String COLOR_BLUE = Stock.IMAGE_FOLDER + "vertex/vertext_color_blue.png";

    private static double lineSize = 1;

    private int i;
    private ImageView imgColor, imgRim;
    private ArrayList<Vertex> edgeTo = new ArrayList<Vertex>();
    private ArrayList<Vertex> edgeFrom = new ArrayList<Vertex>();
    private ArrayList<Line> edges = new ArrayList<Line>();
    private ArrayList<Vertex> subgraph = new ArrayList<Vertex>();

    public Vertex(int x, int y, int i) {
        this.i = i;
        
        createPane(x, y);
        subgraph.add(this);
    }
    public void createPane(int x, int y) {
        Image top = new Image(unselectedImage);
        Image bottom = new Image(COLOR_BLUE);
        Label nodeTitle = new Label("" + i);
        
        imgColor = new ImageView(bottom);
        imgRim = new ImageView(top);

        // TODO
        // Find a better way to place a Label
        nodeTitle.setLayoutX(top.getWidth()/3+2);
        nodeTitle.setLayoutY(top.getWidth()/4);
        
        this.setLayoutX(x - (int)top.getWidth() / 2);
        this.setLayoutY(y - (int)top.getHeight() / 2);
        
        this.getChildren().add(imgColor);
        this.getChildren().add(nodeTitle);
        this.getChildren().add(imgRim);
    }
    public double getMidX() {
        return this.getLayoutX() + (int)imgColor.getImage().getHeight() / 2;
    }
    public double getMidY() {
        return this.getLayoutY() + (int)imgColor.getImage().getWidth() / 2;
    }
    public void setLayoutPosition(int x, int y) {
        this.setLayoutX(x - (int)imgColor.getImage().getWidth() / 2);
        this.setLayoutY(y - (int)imgColor.getImage().getHeight() / 2);
        
        for(int i = 0; i < getEdges().size(); i++) {
            getEdges().get(i).setStartX(getMidX());
            getEdges().get(i).setStartY(getMidY());
        }
        for(int i = 0; i < getEdgeFrom().size(); i++) {
            getEdgeFrom().get(i).moveAbout(this);
        }
    }
    public void setSelected(boolean value) {
        Image select;
        if(value)
            select = new Image(selectedImage);
        else select = new Image(unselectedImage);
        imgRim.setImage(select);
    }
    public ArrayList<Vertex> getEdgeTo() {
        return edgeTo;
    }
    public boolean inEdgeTo(Vertex v) {
        for(Vertex t:getEdgeTo())
            if(t == v)
                return true;
        return false;
    }
    public boolean inEdgeFrom(Vertex v) {
        for(Vertex t:getEdgeFrom())
            if(t == v)
                return true;
        return false;
    }
    public void addEdge(Vertex v, Pane root) {
        if(inEdgeTo(v) || inEdgeFrom(v))
            return;
        
        getEdgeTo().add(v);

        Line l = new Line();
        l.setId(v.getI() + "");
        l.setStartX(getMidX());
        l.setStartY(getMidY());
        l.setEndX(v.getMidX());
        l.setEndY(v.getMidY());
        l.setStrokeWidth(lineSize);
        l.toBack();
        getEdges().add(l);
        v.addEdgeFrom(this);

        root.getChildren().add(l);

        this.toFront();
        v.toFront();
        
        // Check subgraphs
        if(subgraph != v.getSubgraph()) {
            // Merge subgraphs
            for(Vertex sub:v.getSubgraph()) {
                subgraph.add(sub);
                if(sub != v) 
                    sub.subgraph = subgraph;
            }
            v.subgraph = subgraph;
        }
    }
    public void moveAbout(Vertex V) {
        for(int i = 0; i < getEdges().size();i++) {
            if(getEdges().get(i).getId().equals("" + V.getI())) {
                getEdges().get(i).setEndX(V.getMidX());
                getEdges().get(i).setEndY(V.getMidY());
            }
        }
    }
    public ArrayList<Line> getEdges() {
        return edges;
    }
    public int getI() {
        return i;
    }
    public void addEdgeFrom(Vertex v) {
        getEdgeFrom().add(v);
    }
    public boolean isClickingOnImage(MouseEvent me) {
        return (me.getY() < this.getLayoutY() + imgColor.getImage().getHeight() && me.getY() > this.getLayoutY()) && (me.getX() < this.getLayoutX() + imgColor.getImage().getWidth() && me.getX() > this.getLayoutX());
    }
    public double getDistance(MouseEvent me) {
        return Math.sqrt(Math.pow(me.getX() - this.getMidX(), 2) + Math.pow(me.getY() - this.getMidY(), 2));
    }
    public boolean isInBox(Rectangle box) {
        return getMidX() > box.getLayoutX() && getMidX() < (box.getLayoutX() + box.getWidth()) && getMidY() > box.getLayoutY() && getMidY() < (box.getLayoutY() + box.getHeight());
    }
    public boolean isInBounds(int X1, int Y1, int X2, int Y2) {
        return getMidX() > X1 && getMidX() < X2 && getMidY() > Y1 && getMidY() < Y2;
    }
    public double getPixSize() {
        return imgColor.getImage().getHeight(); // Assuming Square
    }
    public static void setLineSize(double lineSize) {
        Vertex.lineSize = lineSize;
    }
    public static double setLineSize() {
        return Vertex.lineSize;
    }
    public ArrayList<Vertex> getSubgraph() {
        return subgraph;
    }
    public void setSubgraph(ArrayList<Vertex> subgraph) {
        this.subgraph = subgraph;
    }
    /**
     * @return the edgeFrom
     */
    public ArrayList<Vertex> getEdgeFrom() {
        return edgeFrom;
    }
}
