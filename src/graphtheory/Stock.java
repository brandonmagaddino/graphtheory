/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphtheory;

import java.util.ArrayList;

/**
 *
 * @author Marx
 */
public class Stock {
    public static final String IMAGE_FOLDER = "file:images/";
    public static final String APP_TITLE = "SOMETHING";
    public static final String MENU_SPACING = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
    public static final String IMAGE_TAB = "file:images/tools/tab.png";
    public static final String IMAGE_TABSELECTED = "file:images/tools/tabselected.png";
    public static final String IMAGE_NEWTAB = "file:images/tools/newtab.png";
    public static final int TAB_Y = 25;
    public static final int INFO_SIZE = 170;
    
    
    
    public static final int[] APP_SIZE = {1200,605}; 
 
    public static String CordToString(int x, int y) {
        return " (" + x + ", " + y + ") ";
    }
    public static String CordToString(double x, double y) {
        return " (" + x + ", " + y + ") ";
    }
    public static ArrayList<ArrayList<Vertex>> getGraphs(ArrayList<Vertex> graph) {
        ArrayList<ArrayList<Vertex>> r = new ArrayList<ArrayList<Vertex>>();
        
        for(Vertex v: graph) {
            if(!r.contains(v.getSubgraph())) {
                r.add(v.getSubgraph());
            }
        }
        return r;
    }
}
