/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphtheory;

import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

/**
 *
 * @author Marx
 */
public class Tab {
    private int i;
    private String title;
    private Pane nodePane;
    private ArrayList<Vertex> vertices;
    
    public Tab(int i) {
        title = "Untitled " + i;
        nodePane = new Pane();
        nodePane.setLayoutY(Stock.TAB_Y + new Image(Stock.IMAGE_NEWTAB).getHeight());
        nodePane.setMinHeight(Stock.APP_SIZE[1] - (Stock.TAB_Y + new Image(Stock.IMAGE_NEWTAB).getHeight()));
        nodePane.setMinWidth(Stock.APP_SIZE[0] - Stock.INFO_SIZE);
        vertices = new ArrayList<Vertex>();
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the nodePane
     */
    public Pane getNodePane() {
        return nodePane;
    }

    /**
     * @param nodePane the nodePane to set
     */
    public void setNodePane(Pane nodePane) {
        this.nodePane = nodePane;
    }

    /**
     * @return the vertices
     */
    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    /**
     * @param vertices the vertices to set
     */
    public void setVertices(ArrayList<Vertex> vertices) {
        this.vertices = vertices;
    }

    /**
     * @return the i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void setI(int i) {
        this.i = i;
    }
}
