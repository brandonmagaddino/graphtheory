/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphtheory;

import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;

/**
 *
 * @author Marx
 */
public class InfoElement {
    public static InfoElement app;
    public static final String title = "";
    private static final int CELL_HEIGHT = 20;
    private Pane root;
    private Pane infoPane;
    
    private ArrayList<String> eleTitle = new ArrayList<String>();
    private ArrayList<String> eleTotal = new ArrayList<String>();
    
    public InfoElement(Pane root) {
        // What its added too
        this.root = root;
        app = this;
        updatePane();
    }
    public void addElement(String identifier, String info) {
        for (int i = 0; i < eleTitle.size(); i++) {
            if (eleTitle.get(i).equals(identifier)) {
                eleTotal.set(i, info);
                updatePane();
                return;
            }
        }
        eleTitle.add(identifier);
        eleTotal.add(info);
        updatePane();
    }
    public void removeElement(String identifier) {
        for (int i = 0; i < eleTitle.size(); i++) {
            if (eleTitle.get(i).equals(identifier)) {
                eleTotal.remove(i);
                eleTitle.remove(i);
                return;
            }
        }
    }
    public void updatePane() {
        if(infoPane != null)
            root.getChildren().remove(infoPane);
        infoPane = new Pane();
        VBox v = new VBox();
        
        
        infoPane.setLayoutX(Stock.APP_SIZE[0]-Stock.INFO_SIZE);
        infoPane.setLayoutY(Stock.TAB_Y);

        infoPane.setMinHeight(Stock.APP_SIZE[1] - (Stock.TAB_Y));// + new Image(Stock.IMAGE_TAB).getHeight()));
        infoPane.setMinWidth(Stock.INFO_SIZE);
        infoPane.setStyle("-fx-background-color: #FFFFFF");
        
        for(int i = 0; i < ((Stock.APP_SIZE[1] - (Stock.TAB_Y))/CELL_HEIGHT);i++){
            Pane cell = new Pane();
            
            cell.setMinHeight(InfoElement.CELL_HEIGHT);
            cell.setMinWidth(Stock.INFO_SIZE);
           
            Label eleTi = new Label((i < eleTitle.size())?eleTitle.get(i):"");
            eleTi.setLayoutY(2);
            eleTi.setLayoutX(15);
            cell.getChildren().add(eleTi);
            
            Label eleTo = new Label((i < eleTitle.size())?eleTotal.get(i):"");
            eleTo.setLayoutY(2);
            eleTo.setLayoutX((Stock.INFO_SIZE * .70) + 15);
            cell.getChildren().add(eleTo);
            
            cell.setStyle("-fx-background-color: #EFF8FB");
            if(i % 2 == 0)
                cell.setStyle("-fx-background-color: #FFFFFF");
            
            v.getChildren().add(cell);
        }
        infoPane.getChildren().add(v);
        root.getChildren().add(infoPane);
    }

    /**
     * @return the eleTitle
     */
    public ArrayList<String> getEleTitle() {
        return eleTitle;
    }

    /**
     * @param eleTitle the eleTitle to set
     */
    public void setEleTitle(ArrayList<String> eleTitle) {
        this.eleTitle = eleTitle;
    }

    /**
     * @return the eleTotal
     */
    public ArrayList<String> getEleTotal() {
        return eleTotal;
    }

    /**
     * @param eleTotal the eleTotal to set
     */
    public void setEleTotal(ArrayList<String> eleTotal) {
        this.eleTotal = eleTotal;
    }
}
