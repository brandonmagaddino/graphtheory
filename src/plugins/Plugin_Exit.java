/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.Vertex;
import java.util.ArrayList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Plugin_Exit extends Plugin {  
    private ArrayList<Vertex> vertices = GUI.app.getVertices();
    private ArrayList<Vertex> selectedVertices = GUI.app.getSelectedVertices();
    private Pane root = GUI.app.getRoot();
    private Stage primaryStage = GUI.app.primaryStage;
    
    @Override
    public void run() {
  // ENGLISH IS THE DEFAULT
        String options[] = new String[]{"Yes", "No"};
        String verifyExit = "Are you sure you want to exit?";

        // FIRST MAKE SURE THE USER REALLY WANTS TO EXIT
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        BorderPane exitPane = new BorderPane();
        HBox optionPane = new HBox();
        Button yesButton = new Button(options[0]);
        Button noButton = new Button(options[1]);
        optionPane.setSpacing(10.0);
        optionPane.getChildren().addAll(yesButton, noButton);
        Label exitLabel = new Label(verifyExit);
        exitPane.setCenter(exitLabel);
        exitPane.setBottom(optionPane);
        Scene scene = new Scene(exitPane, 200, 100);
        dialogStage.setScene(scene);
        dialogStage.show();
        // WHAT'S THE USER'S DECISION?
        yesButton.setOnAction(e -> {
            // YES, LET'S EXIT
            System.exit(0);
        });
        noButton.setOnAction(e -> {
            dialogStage.close();
        });
    }
    public Plugin_Exit() {   
        // Define plugin Information
        setType(Plugin.PLUGIN_OTHER); // Utility because it comes in the Utility menu
        setPluginName("Exit"); // The title of the option
        setPluginHint("Shut down program safely"); // The hover over hint

    }
    public boolean handleClick(MouseEvent mouseEvent) {
        return false; // return true if you used this hook
    }
    public void graphUpdate() {
    }
}