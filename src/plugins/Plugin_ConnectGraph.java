/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.Vertex;
import graphtheory.Stock;
import java.util.ArrayList;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Marx
 */
public class Plugin_ConnectGraph extends Plugin {      
    @Override
    public void run() {
        ArrayList<Vertex> vertices = GUI.app.getVertices();
        ArrayList<Vertex> selectedVertices = GUI.app.getSelectedVertices();
        Pane nodePane = GUI.app.getNodePane();
        
        if(selectedVertices.isEmpty()) {
            for(int i = 0; i < vertices.size();i++) {
                for(int x = i+1; x < vertices.size(); x++) {
                    vertices.get(i).addEdge(vertices.get(x), nodePane);
                }
            }
        } else {
            for(int i = 0; i < selectedVertices.size();i++) {
                for(int x = i+1; x < selectedVertices.size(); x++) {
                    selectedVertices.get(i).addEdge(selectedVertices.get(x), nodePane);
                }
            }
            GUI.app.deselectAll();
        }
    }
    public Plugin_ConnectGraph() {   
        // Define plugin Information
        setType(Plugin.PLUGIN_UTILITY); // Utility because it comes in the Utility menu
        setPluginName("Complete Graph"); // The title of the option
        setPluginHint("This completes the graph (Connects all vertices)"); // The hover over hint

    }
    public boolean handleClick(MouseEvent mouseEvent) {
        return false; // return true if you used this hook
    }
    public void graphUpdate() {
    }
}
