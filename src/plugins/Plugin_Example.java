/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.Stock;
import graphtheory.Vertex;
import java.util.ArrayList;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Marx
 */
public class Plugin_Example extends Plugin {
    private ArrayList<Vertex> vertices = GUI.app.getVertices();
    private ArrayList<Vertex> selectedVertices = GUI.app.getSelectedVertices();
    private Pane root = GUI.app.getRoot();
    private Stage primaryStage = GUI.app.primaryStage;

    public void run() {
        // Runs when user clicks on button
        System.out.println(Stock.getGraphs(vertices).size() + "\n");
        
        // O(n^2) testing :D
        for(int i = 0; i < vertices.size();i++) {
            System.out.print(vertices.get(i).getI() + " part of subgraph: ");
            for(int x = 0; x < vertices.get(i).getSubgraph().size();x++)
                System.out.print(vertices.get(i).getSubgraph().get(x).getI() + " ");
            System.out.println();
        }
        
    }
    public Plugin_Example() {
        // Define plugin Information
        setType(Plugin.PLUGIN_UTILITY); // Utility because it comes in the Utility menu
        setPluginName("PLUGIN EXAMPLE"); // The title of the option
        setPluginHint("Define what it does"); // The hover over hint
    }
    public boolean handleClick(MouseEvent mouseEvent) {
        return false; // return true if you used this hook
    }
    public void graphUpdate() {
    }
}

