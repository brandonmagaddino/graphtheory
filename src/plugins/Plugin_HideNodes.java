/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.Stock;
import graphtheory.Vertex;
import java.util.ArrayList;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Marx
 */
public class Plugin_HideNodes extends Plugin {
    private boolean vis = true;

    public void run() {
        ArrayList<Vertex> vertices = GUI.app.getVertices();
    
        vis = !vis;
        for(int i = 0; i < vertices.size();i++) {
            vertices.get(i).setVisible(vis);
        }
        
    }
    public Plugin_HideNodes() {
        // Define plugin Information
        setType(Plugin.PLUGIN_OPTIONS); // Utility because it comes in the Utility menu
        setPluginName("Hide Nodes [ ]"); // The title of the option
        setPluginHint("Define what it does"); // The hover over hint
    }
    public boolean handleClick(MouseEvent mouseEvent) {
        return false; // return true if you used this hook
    }
    public void graphUpdate() {
        ArrayList<Vertex> vertices = GUI.app.getVertices();
        
        // Enforce last being invis
        for(int i = 0; i < vertices.size();i++) {
            vertices.get(i).setVisible(vis);
        }
    }
}

