/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.Vertex;
import graphtheory.Stock;
import java.util.ArrayList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Marx
 */
public class Plugin_Populate extends Plugin {    
    @Override
    public void run() {      
        populateBuild();
    }
    public void populateBuild() {
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(GUI.app.primaryStage);
        Pane root = new Pane();
        Label lbl = new Label("Enter the amount of vertices to add: ");
        TextField txt = new TextField("25");
        Button btnConfirm = new Button("Apply");
        Button btnCancle = new Button("Cancle");
        root.getChildren().addAll(btnConfirm,btnCancle,lbl,txt);
        Scene scene = new Scene(root, 250, 75);
        
        lbl.setLayoutX(5);
        lbl.setLayoutY(10);
        txt.setLayoutX(205);
        txt.setLayoutY(5);
        txt.setMaxWidth(40);
        btnConfirm.setMaxWidth(55);
        btnConfirm.setMaxHeight(25);
        btnConfirm.setMinWidth(btnConfirm.getMaxWidth());
        btnConfirm.setMinHeight(btnConfirm.getMaxHeight());
        btnConfirm.setLayoutX(5);
        btnConfirm.setLayoutY(scene.getHeight()- (5 + btnConfirm.getMaxHeight()));
        btnCancle.setMaxWidth(btnConfirm.getMaxWidth());
        btnCancle.setMaxHeight(btnConfirm.getMaxHeight());
        btnCancle.setMinWidth(btnConfirm.getMaxWidth());
        btnCancle.setMinHeight(btnConfirm.getMaxHeight());
        btnCancle.setLayoutX(btnConfirm.getMaxWidth() + 5 * 2);
        btnCancle.setLayoutY(scene.getHeight() - (5 + btnConfirm.getMaxHeight()));
        
        dialogStage.setScene(scene);
        dialogStage.show();
        
        btnConfirm.setOnAction(e -> {
            try {
                populate(Integer.parseInt(txt.getText()));
            } catch (Exception exc) {
                exc.printStackTrace();

            }
            dialogStage.close();
        });
        btnCancle.setOnAction(e -> {
            dialogStage.close();
        });
    }
    public void populate(int amount) {
        ArrayList<Vertex> vertices = GUI.app.getVertices();
        Pane nodePane = GUI.app.getNodePane();
        int from = vertices.size();
        
        for(int i = 0; i < amount;i++) {
            Vertex v = new Vertex(0,0,vertices.size());
            vertices.add(v);
            nodePane.getChildren().add(v);
        }
        circleUp(from);
    }
    public Plugin_Populate() {
        // Define plugin Information
        setType(Plugin.PLUGIN_TOOL); // Utility because it comes in the Utility menu
        setPluginName("Populate Graph"); // The title of the option
        setPluginHint("Populates and arranges X amount of vertices"); // The hover over hint

    }
    public void circleUp(int from) {
        ArrayList<Vertex> vertices = GUI.app.getVertices();
        Pane nodePane = GUI.app.getNodePane();
        
        double CY = nodePane.getHeight() / 2;
        double CX = nodePane.getWidth() / 2;
        double radius = (vertices.get(0).getPixSize() / 5) * (vertices.size() - from);

        for (int i = from; i < vertices.size(); i++) {
            double o = (2 * Math.PI) * i / (vertices.size() - from) + (1.5 * Math.PI);
            int x = (int) Math.round(CX + radius * Math.cos(o));
            int y = (int) Math.round(CY + radius * Math.sin(o));

            vertices.get(i).setLayoutPosition((int) x, (int) y);
        }
    }
    public boolean handleClick(MouseEvent mouseEvent) {
        return false; // return true if you used this hook
    }
    public void graphUpdate() {
    }
}
