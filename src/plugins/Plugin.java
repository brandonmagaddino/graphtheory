/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.Vertex;
import graphtheory.Stock;
import java.io.File;
import java.util.ArrayList;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Marx
 */
public abstract class Plugin {
    private static final String DIRECTORY = "src\\plugins\\";

    public static final String[] PLUGIN_TYPES = {"File", "Tools", "Utilities", "Arrange", "Options"};
    
    public static final int PLUGIN_FILE = 0;
    public static final int PLUGIN_TOOL = 1;
    public static final int PLUGIN_UTILITY = 2;
    public static final int PLUGIN_ARRANGE = 3;
    public static final int PLUGIN_OPTIONS = 4;
    public static final int PLUGIN_DEFAULT = 5;
    public static final int PLUGIN_OTHER = 6;
    

    private int type;
    private static ArrayList<Plugin> plugins;
    private String pluginName;
    private String pluginHint;
    
    public abstract void run();
    public abstract boolean handleClick(MouseEvent mouseEvent);
    public abstract void graphUpdate();
    
    
    public Plugin() {
        type = PLUGIN_OTHER;
    }
    public static ArrayList<Plugin> loadPlugins() {
        if (plugins != null)
            return plugins;
                
        plugins = new ArrayList<Plugin>();
        
        try {
            for (File l : new File(DIRECTORY).listFiles()) {
                String pluginName = l.getName().substring(0, l.getName().lastIndexOf('.'));
                
                if (pluginName.contains("Plugin_")) {
                    Class plugin = Class.forName("plugins." + pluginName);
                    Plugin z = (Plugin) plugin.newInstance();
                    plugins.add(z);
                    System.out.println("Load Plugin: " + z.getPluginName());
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        
        return plugins;
    }
    public void setType(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }
    public String getPluginName() {
        return pluginName;
    }
    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }
    public String getPluginHint() {
        return pluginHint;
    }
    public void setPluginHint(String pluginHint) {
        this.pluginHint = pluginHint;
    }
}
