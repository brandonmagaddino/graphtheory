/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.Vertex;
import java.util.ArrayList;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Marx
 */
public class Plugin_ArrangeCircle extends Plugin {   
    @Override
    public void run() {
        ArrayList<Vertex> vertices = GUI.app.getVertices();
        ArrayList<Vertex> selectedVertices = GUI.app.getSelectedVertices();
        Pane nodePane = GUI.app.getNodePane();
        
        if(vertices.isEmpty())
            return;
        
        if(selectedVertices.isEmpty()) {
            // Get center x/y
            double CY = nodePane.getHeight() / 2;
            double CX = nodePane.getWidth() / 2;
            double radius = (vertices.get(0).getPixSize() / 5) * vertices.size();

            for(int i = 0; i < vertices.size();i++) {
                double o = (2 * Math.PI) * i / vertices.size() + (1.5 * Math.PI);
                int x = (int) Math.round(CX + radius * Math.cos(o));
                int y = (int) Math.round(CY + radius * Math.sin(o));

                vertices.get(i).setLayoutPosition((int)x,(int) y);
            }
        } else {
            double CY = nodePane.getHeight() / 2;
            double CX = nodePane.getWidth() / 2;
            double radius = (selectedVertices.get(0).getPixSize() / 5) * selectedVertices.size();

            for(int i = 0; i < selectedVertices.size();i++) {
                double o = (2 * Math.PI) * i / selectedVertices.size() + (1.5 * Math.PI);
                int x = (int) Math.round(CX + radius * Math.cos(o));
                int y = (int) Math.round(CY + radius * Math.sin(o));

                selectedVertices.get(i).setLayoutPosition((int)x,(int) y);
            }
            GUI.app.deselectAll();
        }
    }
    public Plugin_ArrangeCircle() {   
        // Define plugin Information
        setType(Plugin.PLUGIN_ARRANGE); // Utility because it comes in the Utility menu
        setPluginName("Circle Configuration"); // The title of the option
        setPluginHint("This puts the graph around the center of the pane evenly"); // The hover over hint
    }
    public boolean handleClick(MouseEvent mouseEvent) {
        return false; // return true if you used this hook
    }
    public void graphUpdate() {
    }
}
