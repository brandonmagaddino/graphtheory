/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins;

import graphtheory.GUI;
import graphtheory.InfoElement;
import graphtheory.Stock;
import graphtheory.Vertex;
import java.util.ArrayList;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Marx
 */
public class Plugin_BasicInfo extends Plugin {
    public void run() {
    }
    public Plugin_BasicInfo() {
        // Define plugin Information
        setType(Plugin.PLUGIN_DEFAULT); // Utility because it comes in the Utility menu
        setPluginName("Basic Information"); // The title of the option
        setPluginHint("N/A"); // The hover over hint
    }
    public boolean handleClick(MouseEvent mouseEvent) {
        return false; // return true if you used this hook
    }
    public void graphUpdate() {
        ArrayList<Vertex> vertices = GUI.app.getVertices();
        InfoElement info = InfoElement.app;
        
        int edges = 0;
        for(int i = 0; i < vertices.size();i++) {
            edges += vertices.get(i).getEdgeTo().size();
        }
        
        info.addElement("Vertices", vertices.size() + "");
        info.addElement("Edges", edges + "");
        info.addElement("Graphs", Stock.getGraphs(vertices).size() + "");
    }
}

